﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TestEncryptionApplication.Models;
using TestEncryptionApplication.ServiceReference1;

namespace TestEncryptionApplication.Controllers
{
    public class PersonneController : Controller
    {
        /// <summary>
        /// Liste des personnes ajoutées
        /// </summary>
        /// <returns></returns>
        public ActionResult PersonnePage()
        {
            List<Personne> personnes = PersonneModel.GetPersonnes();

            return View(personnes);
        }

        /// <summary>
        /// Recherche sur une personne
        /// </summary>
        /// <param name="recherche"></param>
        /// <returns></returns>
        public ActionResult PersonneFilterPage(string recherche = null)
        {
            List<Personne> personnes = new List<Personne>();

            //Si recherche
            if (!String.IsNullOrEmpty(recherche))
            {
                personnes = PersonneModel.GetAllPersonnesByNom(recherche);
            }
            else{
                personnes = PersonneModel.GetPersonnes();
            }

            return View("~/Views/Personne/PersonnePage.cshtml",  personnes);
        }

        /// <summary>
        /// Page de création d'une personne
        /// </summary>
        /// <returns></returns>
        public ActionResult CreationPersonne()
        {
            return View(new Personne());
        }

        /// <summary>
        /// Envoie du formulaire depuis la page de création
        /// </summary>
        /// <param name="personne"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreationPersonneForm(Personne personne)
        {
            int resultat = PersonneModel.AddPersonne(personne.nom, personne.prenom, personne.date_naissance);

            return RedirectToAction("PersonnePage");
        }
    }
}