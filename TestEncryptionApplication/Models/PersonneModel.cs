﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestEncryptionApplication.ServiceReference1;

namespace TestEncryptionApplication.Models
{
    public class PersonneModel
    {
        static Service1Client db = new Service1Client();

        /// <summary>
        /// Fonction permettant d'ajouter les informations d'une personne
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <param name="dateNaissance"></param>
        /// <returns></returns>
        public static int AddPersonne(string nom, string prenom, DateTime dateNaissance)
        {
            return db.AddPersonne(nom, prenom, dateNaissance);
        }

        /// <summary>
        /// Fonction retourant la liste des personnes par nom
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public static List<Personne> GetAllPersonnesByNom(string nom)
        {
            return db.GetPersonneByName(nom);
        }

        /// <summary>
        /// Fonction retourant la liste de toutes les personnes
        /// </summary>
        /// <returns></returns>
        public static List<Personne> GetPersonnes()
        {
            return db.GetPersonnes();
        }

        /// <summary>
        /// Fonction retournant une personne par son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Personne GetPersonneById(int id)
        {
            return db.GetPersonneById(id);
        }
    }
}